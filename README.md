# README #

### What is this repository for? ###
Onlinemeded Engineering Training

Specifically, hands-on training for working with Kubernetes and Jenkins Pipelines to get a working application running under the CICD paradigm.  While the application is simple, the principles apply to any code base, and can be adapted for use by teams here at Onlinemeded.

The training environment consists of the following components, all running under Kubernetes provided by Docker Desktop.

* Docker Desktop with Kubernetes running as the deployment K8s cluster for the application
* Jenkins server
* GitLab CE server to serve as the repository for the codebase and the pipeline
* An application called __SuperApp__ - a simple HTML page served by the Python http server

### How do I get set up? ###

* Check out this repository
* Enable Kubernetes under Docker Desktop
* Refer to the lab exercises in the PDF folder

### Contribution guidelines ###

* Feel free to put in a PR for improvements/edits to these files!

### Who do I talk to? ###

* see David Ross and/or the DevOps team for more information or if you have questions

### Configuration Notes ###
These are references for the DevOps team.

## Not used in the training, reference only ##
#### Generate a self-signed certificate for the registry server ####
These files are under __etc/k8s/devops/registry/config__

```openssl req \
  -newkey rsa:2048 -nodes -sha256 -keyout domain.key \
  -x509 -days 3650 -out domain.crt

Generating a 2048 bit RSA private key
........................................+++
...........................................................+++
writing new private key to 'domain.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) []:US
State or Province Name (full name) []:TX
Locality Name (eg, city) []:Austin
Organization Name (eg, company) []:OnlineMedEd
Organizational Unit Name (eg, section) []:DevOps
Common Name (eg, fully qualified host name) []:registry-svc.k8straining.svc.cluster.local
Email Address []:admin@devops  
  ```
#### Generate a username/password for authentication to the registry ####
You have to use a slightly older image due to a vulnerability in CVE - they have removed __htpasswd__ from new images until it is fixed. See: (https://github.com/docker/distribution-library-image/issues/106)

``` docker run \
  --entrypoint htpasswd \
  registry:2.7.0 -Bbn training object00 > htpasswd
  ```